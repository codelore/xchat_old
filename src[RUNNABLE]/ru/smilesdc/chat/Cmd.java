package ru.smilesdc.chat;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.iCo6.system.Account;
import com.iCo6.system.Holdings;

public class Cmd implements CommandExecutor {

	@Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(!(sender instanceof Player))
  			  return false;
	  	Player player = (Player) sender;                
		PermissionUser user = PermissionsEx.getPermissionManager().getUser(player);
	  	if(cmd.getName().equalsIgnoreCase("g")) {
	  		  if(!player.hasPermission("chat.g.command")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
	  		  }
	  		  if(args.length != 0) {
	  			  player.sendMessage(ChatColor.RED + "����������: /g. ����� ����� �� �������������� � ����� ����������� ����.");
	  			return true;
	  		  }
	  		  String msg = "";
	  		  Chat.chache.set(player.getName(), "g");
	  		  player.sendMessage(ChatColor.GREEN + "�� ������������� � ���������� ���!");
	  		  return true;
	  	  }
	  	  //====================================================================================
	  	  else if(cmd.getName().equalsIgnoreCase("t")) {
	  		  if(!player.hasPermission("chat.t.command")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
	  		  }
	  		  if(args.length != 0) {
	  			player.sendMessage(ChatColor.RED + "����������: /t. ����� ����� �� �������������� � ����� ��������� ����.");
	  			return true;
	  		  }
	  		  Chat.chache.set(player.getName(), "t");
	  		  player.sendMessage(ChatColor.GREEN + "�� ������������� � �������� ���!");
	  		  return true;
	  	  }
	  	  //====================================================================================
	  	  else if(cmd.getName().equalsIgnoreCase("h")) {
	  		  if(!player.hasPermission("chat.h.command")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
	  		  }
	  		  if(args.length != 0) {
	  			  player.sendMessage(ChatColor.RED + "����������: /h. ����� ����� �� �������������� � ���� ������.");
	  			return true;
	  		  }
	  		Chat.chache.set(player.getName(), "h");
	  		  player.sendMessage(ChatColor.GREEN + "�� ������������� � ��� ������!");
	  		  return true;
	  	  }
	  	  //====================================================================================
	  	  else if(cmd.getName().equalsIgnoreCase("a")) {
	  		  if(!player.hasPermission("chat.a.command")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
	  		  }
	  		  if(args.length != 0) {
	  			  player.sendMessage(ChatColor.RED + "����������: /a. ����� ����� �� �������������� � ����� ���� ���������������.");
	  			  return true;
	  		  }
	  		Chat.chache.set(player.getName(), "a");
	  		  player.sendMessage(ChatColor.GREEN + "�� ������������� � ��� ���������������!");
	  		  return true;
	  	  }
                 
	  //====================================================================================
  		  else if(cmd.getName().equalsIgnoreCase("l")) {
  			  if(!player.hasPermission("chat.l.command")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
  			  }
  		      Chat.chache.set(player.getName(), "def");
  		      player.sendMessage("#ADFF2F"+ "��� ����� ���� �������!");
  		      return true;
  		  }       
	  //====================================================================================
  		  else if(cmd.getName().equalsIgnoreCase("list")|| cmd.getName().equalsIgnoreCase("who")) {
  			  if(!player.hasPermission("rc.list")) {
	  			  player.sendMessage("#A52A2A" + "� ��� ��� ���� ��� �����!");
	  			  return true;
  			  }
  			  StringBuilder b = new StringBuilder();
  			  b.append(ChatColor.GREEN + "�� ������� "+ChatColor.RED + Bukkit.getOnlinePlayers().length +ChatColor.GREEN + " ������� �� "+ChatColor.RED +
  			  Bukkit.getMaxPlayers() +ChatColor.GREEN+ " ���������: " + ChatColor.WHITE + "\n");
  			  int i = 1;
  			  for(Player p : Bukkit.getOnlinePlayers()) {
                                  user = PermissionsEx.getUser(p);
  				  if(Chat.chache.getBoolean(p.getName() + ".show", true) || player.hasPermission("rc.showall")) {
  					  String prefix = "";
  					  if(!(user.getOwnPrefix() == null))
  						  prefix = user.getOwnPrefix();
                                                  prefix = Chat.translateColorCodes(prefix);
  						  if(i == Bukkit.getOnlinePlayers().length) 
  							  b.append(prefix + p.getDisplayName() + ChatColor.WHITE + ".");
  						  else 
  							  b.append(prefix + p.getDisplayName() + ChatColor.WHITE + ", ");
  				  }
  				  i++;
  			  }
  			  player.sendMessage(b.toString());
  			  return true;
  		  }
	  //====================================================================================
  		  else if(cmd.getName().equalsIgnoreCase("hide")) {
  			  if(!player.hasPermission("rc.hide"))
  				  return true;
                          user = PermissionsEx.getUser(player);
		          String prefix = user.getOwnPrefix();
                          String suffix = user.getOwnSuffix();
                          player.setPlayerListName("");
  			  player.setDisplayName("");
                          prefix = "";
                          suffix = "";
  			  Chat.hidePl.add(player);
                          Chat.chache.set(player.getName() + ".show", false);
                          player.sendMessage(ChatColor.GREEN + "You're hidden from list, map and no submitting messages now!");
  			  return true;
  		  }
	  //====================================================================================
                  else if(cmd.getName().equalsIgnoreCase("show")){
                      if(!player.hasPermission("rc.show"))
                           return true;
                      {   
                          user = PermissionsEx.getUser(player);
		          String prefix = user.getOwnPrefix();
                          String suffix = user.getOwnSuffix();
                          prefix = user.getPrefix();
                          suffix = user.getSuffix();
                          player.setDisplayName(player.getName());
                          player.setPlayerListName(player.getDisplayName());
                          Chat.chache.set(player.getName() + ".show", true);
                          player.sendMessage(ChatColor.GREEN+ "You're shows in list, map and submitting messages now!");
                          return true;
                      }
                  }
          //====================================================================================
  		  else if(cmd.getName().equalsIgnoreCase("m") || cmd.getName().equalsIgnoreCase("msg")) {
  			  if(!player.hasPermission("rc.m"))
  				  return true;
  			  if(args.length < 2) {
  				  player.sendMessage(ChatColor.RED + "/m <�����> <���������>");
  				  return true;
  			  }
  			  Player to = getPl(args[0]);
  			  if(to == null)
  				  return true;
  			  if(!Chat.chache.getBoolean(to.getName() + ".show", true))
  				  return true;
                          String msg = "";
  			  int i = 0;
  			  for(String s : args) {
  				  if(i != 0)
  					  msg += s;                                         
  		  }
                          player.sendMessage("["+player.getDisplayName()+"] -> ["+to.getName()+"]: "+ChatColor.GRAY+ msg);
  			  to.sendMessage("["+ player.getDisplayName()+"] -> [" +to.getName()+"]: " + ChatColor.GRAY + msg.toString());
                          Chat.write(new File(Chat.df, "privatemessage.log"), ("["+ player.getDisplayName()+"] -> [" +to.getName()+"]: " + msg));
  			  return true;
  		  }
	  	  return false;
	}
	
	Player getPl(String name) {
		List<String> pl = new LinkedList<String>();
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.getName().toLowerCase().startsWith(name.toLowerCase())) {
				return p;
			}
		}
		return null;
	}

}
