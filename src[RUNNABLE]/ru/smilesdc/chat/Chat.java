package ru.smilesdc.chat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import ru.tehkode.permissions.PermissionUser;

/**
 * ChatPlugin special for RU-Craft.NET
 * @author smilesdc
 * @version 1.0
 */
public class Chat extends JavaPlugin implements Listener {
	
	public static List<Player> hidePl;
	public static FileConfiguration chache;
	public static File f;
	public static final Logger log  = Logger.getLogger("Minecraft");
	public static File df;
	
	protected static Pattern chatColorPattern = Pattern.compile("(?i)&([0-9A-F])");
	protected static Pattern chatMagicPattern = Pattern.compile("(?i)&([K])");
	protected static Pattern chatBoldPattern = Pattern.compile("(?i)&([L])");
	protected static Pattern chatStrikethroughPattern = Pattern.compile("(?i)&([M])");
	protected static Pattern chatUnderlinePattern = Pattern.compile("(?i)&([N])");
	protected static Pattern chatItalicPattern = Pattern.compile("(?i)&([O])");
	protected static Pattern chatResetPattern = Pattern.compile("(?i)&([R])");
        
      public void onEnable() {
    	  hidePl = new LinkedList<Player>();
    	  df = getDataFolder();
    	  f = new File(df, "chache.yml");
    	  chache = YamlConfiguration.loadConfiguration(f);
    	  PluginManager pm = getServer().getPluginManager();
    	  pm.registerEvents(new chatListener(), this);
    	  Cmd c = new Cmd();
    	  getCommand("g").setExecutor(c);
    	  getCommand("t").setExecutor(c);
    	  getCommand("h").setExecutor(c);
    	  getCommand("a").setExecutor(c);
    	  getCommand("l").setExecutor(c);
          getCommand("hide").setExecutor(c);
          getCommand("m").setExecutor(c);
          getCommand("list").setExecutor(c);
          getCommand("msg").setExecutor(c);
          getCommand("show").setExecutor(c);
          getCommand("who").setExecutor(c);
    	  log("Enabled!");
      }
      
      public void onDisable(){
    	  log("Disabled!");
      }
      
      void log(String msg) {
    	  log.info("[] RU-Craft Chat " + msg);
      }
  	public static String translateColorCodes(String string) {
		if (string == null) {
			return "";
		}

		String newstring = string;
		newstring = chatColorPattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatMagicPattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatBoldPattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatStrikethroughPattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatUnderlinePattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatItalicPattern.matcher(newstring).replaceAll("\u00A7$1");
		newstring = chatResetPattern.matcher(newstring).replaceAll("\u00A7$1");
		return newstring;
	}
/*  	protected String translateColorCodes(String string, PermissionUser user, String worldName) {
		if (string == null) {
			return "";
		}

		String newstring = string;
		if (user.has(permissionChatColor, worldName)) {
			newstring = chatColorPattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		if (user.has(permissionChatMagic, worldName)) {
			newstring = chatMagicPattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		if (user.has(permissionChatBold, worldName)) {
			newstring = chatBoldPattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		if (user.has(permissionChatStrikethrough, worldName)) {
			newstring = chatStrikethroughPattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		if (user.has(permissionChatUnderline, worldName)) {
			newstring = chatUnderlinePattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		if (user.has(permissionChatItalic, worldName)) {
			newstring = chatItalicPattern.matcher(newstring).replaceAll("\u00A7$1");
		}
		newstring = chatResetPattern.matcher(newstring).replaceAll("\u00A7$1");
		return newstring;
	}*/
  	@SuppressWarnings("deprecation")
	public static void write(File file, String msg) {
  		FileWriter fw = null;
		try {
			fw = new FileWriter(file, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
  		Date d = new Date();
  		msg = "[" + d.getYear() + "_" + d.getMonth() + "_" + d.getDay() + "_" + d.getHours() + "_" + d.getMinutes() + "_" + d.getSeconds() + "]" +
  		msg + "\r\n";
  		try {
			fw.write(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
                  try {
                            fw.close();
                  } catch (IOException e) {
                  e.printStackTrace();
            }
  	}
      
}
