package ru.smilesdc.chat;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.iCo6.system.Account;
import com.iCo6.system.Holdings;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

@SuppressWarnings("deprecation")
    public class chatListener implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
                event.setJoinMessage("");               
		Chat.chache.set(p.getName(), "def");
		try {
			Chat.chache.save(Chat.f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@EventHandler
        public void onPlayerQuit(PlayerQuitEvent event){
            event.setQuitMessage("");
        }
	@EventHandler
    public void onPlayerChat(PlayerChatEvent event) { 
            if(event.isCancelled())
            return;
   	double range = 150.0;
        Player player = event.getPlayer();
		  if(!Chat.chache.getString(player.getName()).equalsIgnoreCase("def")) {
                       event.setFormat("");
                       event.getRecipients().clear();
                       String channel = Chat.chache.getString(player.getName());
                       if(channel.equalsIgnoreCase("g"))
                    	   global(player, event.getMessage());
                       if(channel.equalsIgnoreCase("t"))
                    	   torg(player, event.getMessage());
                       if(channel.equalsIgnoreCase("h"))
                    	   help(player, event.getMessage());
                       if(channel.equalsIgnoreCase("a"))
                    	   admin(player, event.getMessage());
                       return;
		  }	
		        PermissionUser user = PermissionsEx.getPermissionManager().getUser(player);
		        String prefix = user.getOwnPrefix();
                        String suffix = user.getOwnSuffix();
			prefix = Chat.translateColorCodes(prefix);
			if(prefix == null)
				prefix = user.getPrefix();
			if(prefix == null)
				prefix = "";       
			event.getRecipients().clear();
        for(Player r : getLocalRecipients(player, range)) {
        	if(!r.equals(player))
        		r.sendMessage(prefix + player.getDisplayName() + ChatColor.WHITE +": " + event.getMessage());
        	else
        		player.sendMessage(ChatColor.WHITE+"("+ "#87CEFA" + (getLocalRecipients(player, range).size() - 1) + ChatColor.WHITE + ") "+ prefix + player.getDisplayName() + ChatColor.WHITE +": "+ suffix  + event.getMessage());
        }
        event.setFormat("");
        Chat.write(new File(Chat.df, "local.log"), (player.getName() + ": " + event.getMessage()));
}


    protected List<Player> getLocalRecipients(Player sender, double range) {
        Location playerLocation = sender.getLocation();
        List<Player> recipients = new LinkedList<Player>();
        double squaredDistance = Math.pow(range, 2);
        for (Player recipient : Bukkit.getServer().getOnlinePlayers()) {
        if(!recipient.getWorld().equals(sender.getWorld())) {
        continue;
        }
        if (playerLocation.distanceSquared(recipient.getLocation()) > squaredDistance) {
        continue;
        }
        recipients.add(recipient);
        }
        return recipients;
    }
    
    void global(Player p, String msg) {
		PermissionUser user = PermissionsEx.getPermissionManager().getUser(p);
		String prefix = user.getOwnPrefix();
		if(p.hasPermission("chat.color"))
		        prefix = Chat.translateColorCodes(prefix);
		if(prefix == null)
			prefix = user.getPrefix();
		if(prefix == null)
			prefix = "";
    	for(Player s : Bukkit.getOnlinePlayers())
		  s.sendMessage(ChatColor.WHITE + "["+ChatColor.GREEN+"G"+ChatColor.WHITE+ "] " + prefix + p.getDisplayName() + ChatColor.WHITE +": " + ChatColor.YELLOW + msg);
    	Chat.write(new File(Chat.df, "global.log"), (p.getName() + ": " + msg));
    }
    
    void torg(Player p, String msg) {
		  Account acc = new Account(p.getName());
		  Holdings h = acc.getHoldings();
		  Double b = h.getBalance();
		  if(b < 25.0) {
			  p.sendMessage("#A52A2A" + "� ��� �� ������� �����!");
			  return;
		  }
		  h.setBalance(b - 25.0);
			PermissionUser user = PermissionsEx.getPermissionManager().getUser(p);
			String prefix = user.getOwnPrefix();
			if(p.hasPermission("chat.color")) {
			        prefix = Chat.translateColorCodes(prefix);
			        msg = Chat.translateColorCodes(msg);
			}
			if(prefix == null)
				prefix = user.getPrefix();
			if(prefix == null)
				prefix = "";
  		  for(Player s : Bukkit.getOnlinePlayers())
  			  s.sendMessage(ChatColor.WHITE + "[" + ChatColor.GOLD + "T" + ChatColor.WHITE + "] " +
  					  ChatColor.WHITE + prefix + p.getDisplayName() + ChatColor.WHITE + ": " + ChatColor.GOLD + msg);
		  Chat.write(new File(Chat.df, "torg.log"), (p.getName() + ": " + msg));
    }
    
    void help(Player p, String msg) {
		  for(Player s : Bukkit.getOnlinePlayers()) {
			  if((Chat.chache.getString(s.getName()).equalsIgnoreCase("h"))  || s.hasPermission("chat.h.moder"))
				  s.sendMessage(ChatColor.WHITE + "[" + ChatColor.GREEN + "H" + ChatColor.WHITE + "] " +
  					  p.getDisplayName() + ChatColor.WHITE + ": " + ChatColor.DARK_AQUA + msg);
		  }
  		Chat.write(new File(Chat.df, "help.log"), (p.getName() + ": " + msg));
    }
    
    void admin(Player p, String msg) {
		  for(Player s : Bukkit.getOnlinePlayers()) {
  			  if(s.hasPermission("chat.a.command"))
  				s.sendMessage(ChatColor.WHITE + "[" + ChatColor.GREEN + "A" + ChatColor.WHITE + "] " +
  			  		 p.getDisplayName() + ChatColor.WHITE + ": " + ChatColor.RED + msg);
		  }
	  	 Chat.write(new File(Chat.df, "admin.log"), (p.getName() + ": " + msg));
    }
}
